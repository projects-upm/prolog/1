:- module(_, _, [assertions]).

alumno_prode('Lopez', 'Rosende', 'Jorge', 'A180499').

:- doc(title, "Memoria y documentacion - Practica 1").

:- doc(author, "Jorge Lopez Rosende").

:- doc(module, "En esta practica se definen funciones para trabajar con edificios que contengan niveles y viviendas.

@section{Estructuras basicas}
Los niveles estan definidos de esta forma @tt{[ s(0), s(s(s(0))) ]}
y los edificios de la forma @tt{[ [ s(0), s(s(s(0))) ], [ 0, s(s(0)) ] ]}.

Existen dos tipos de eificios los basic_building que se definen con al menos un piso y una vivienda.
siendo el edificio minimo @tt{[ [ 0 ] ]}.

El otro tipo los buildings tienen como base un basic_building y se anade la regla de que los pisos 
tienen que tener el mismo numero de viviendas.

@section{Funciones auxiliares}

Se ha creado una lista de funciones auxiliares para facilitar la realizacion de la practica.

@section{Funciones principales}

Se han desarrollado usando solamente aritmetica de peano.

Tambien se han creado test que evaluan los predicados y las propiedades para evaluar detectar errores en la programacion.


@section{Tests automáticos}
Este módulo incluye aserciones que empiezan por @tt{% :- test}. Por ejemplo
@begin{verbatim}
:- test basic_building(X) : (X = [[0]]) + not_fails #``Caso base''.
@end{verbatim}

Estas definen casos de test. Dada una aserción @tt{% :- test Cabeza : Llamada =>
Salida + Comp}, @var{Cabeza} denota a qué predicado se refiere la aserción,
@var{Llamada} describe los valores de entrada para el test, @var{Salida} define
los valores de salida @bf{si el predicado tiene éxito} y @var{Comp} lo vamos a
usar para definir si el predicado tiene que tener éxito para esa llamada o no:
@begin{itemize}
@item @tt{not_fails}: significa que la llamada al predicado con la entrada @var{Llamada} siempre tendrá al menos una solución.
@item @tt{fails}: significa que la llamada al predicado con la entrada @var{Llamada} siempre fallará.
@end{itemize}

@subsection{Lanzar los test automáticamente}
Para lanzar los test, selecciona en el menú de emacs @tt{CiaoDbg -> Run tests in current module}.

").

:- prop natural/1 #"Numero natural que representan el numero de personas que viven en una vivienda. @includedef{natural/1}".
natural(0).
natural(s(X)) :- natural(X).

:- prop vivienda(X) #"Define una vivienda con un numero depersonas en la @var{X}. @includedef{vivienda/1}".
vivienda(X) :- natural(X).

:- prop piso(X) #"Define un piso como una lista que contiene varias viviendas en la @var{X}. @includedef{piso/1}".
piso([X]) :- vivienda(X).
piso([E|L]) :- vivienda(E),piso(L).

:- prop basic_building(X) #"Define un basic building en la @var{X}. Comprueba que tenga al menos un piso y al menos una vivienda en cada piso. @includedef{basic_building/1}".
basic_building([X]) :- piso(X).
basic_building([E|X]) :- piso(E),basic_building(X).

:- test basic_building(X) : (X = [[0]]) + not_fails #"Caso base".
:- test basic_building(X) : (X = [[]]) + fails #"Caso erroneo".
:- test basic_building(X) : (X = [[0,s(0)]]) + not_fails #"Multiples viviendas".
:- test basic_building(X) : (X = [[0,0],[0]]) + not_fails #"Multiples pisos".
:- test basic_building(X) : (X = [[0,0],[]]) + fails #"Multiples pisos y uno sin viviendas".

:- prop building(X) #"Define un building en la @var{X}. @includedef{building/1}".
building(X) :- building_same(X,_).

:- test building(X) : (X = [[0]]) + not_fails #"Caso base".
:- test building(X) : (X = [[]]) + fails #"Caso erroneo".
:- test building(X) : (X = [[0,s(0)]]) + not_fails #"Multiples viviendas".
:- test building(X) : (X = [[0,0],[0]]) + fails #"Multiples pisos incompletos".
:- test building(X) : (X = [[0,0],[]]) + fails #"Multiples pisos incompletos".
:- test building(X) : (X = [[0,0],[0,0]]) + not_fails #"Multiples pisos con el mismo numero de viviendas en cada piso".

:- pred level(X,N,C) #"Retorna en @var{C} el piso del nivel @var{N} en el building @var{X}. @includedef{level/3}".
level(X,s(N),C) :- building(X),get_nth(X,N,C).

:- test level(X,N,C) : (X = [[s(0),0],[0,0]], N = 0) + fails #"No puede retornar el piso 0 por que no existe".
:- test level(X,N,C) : (X = [[s(0),0],[0,0]], N = s(0)) => (C = [s(0),0]) + not_fails #"Retorna el piso del numero especificado".
:- test level(X,N,C) : (X = [[s(0),0],[0,s(0)]], N = s(s(0))) => (C = [0,s(0)]) + not_fails #"Retorna el piso del numero especificado".
:- test level(X,N,C) : (X = [[s(0),0],[0,0]], N = s(s(s(0)))) + fails #"No puede retornar el piso 3 por que no existe".

:- pred column(X,N,C) #"Retorna en @var{C} una lista de las viviendas de la columna @var{N} en el building @var{X}. @includedef{column/3}".
column([E|L],s(N),C) :- columns([E|L],X),get_nth(X,N,C).

:- test column(X,N,C) : (X = [[s(0),0],[0,0]], N = 0) + fails #"No puede retornar la columna 0 por que no existe".
:- test column(X,N,C) : (X = [[s(0),s(0)],[0,0]], N = s(0)) => (C = [s(0),0]) + not_fails #"Retorna la columna del numero especificado".
:- test column(X,N,C) : (X = [[s(0),s(0)],[0,s(0)]], N = s(s(0))) => (C = [s(0),s(0)]) + not_fails #"Retorna la columna del numero especificado".
:- test column(X,N,C) : (X = [[s(0),0],[0,0]], N = s(s(s(0)))) + fails #"No puede retornar la columna 3 por que no existe".

:- pred columns(X,C) #"Retorna en @var{C} las columnas del building @var{X}. @includedef{columns/2}".
columns([], []).
columns([E|L], C) :- building([E|L]), columns_trans(E, [E|L], C).

:- test columns(X,C) : (X = [[s(0),s(s(0))],[s(s(s(0))),s(s(s(s(0))))]]) => (C = [[s(0),s(s(s(0)))],[s(s(0)),s(s(s(s(0))))]])+ not_fails #"No puede retornar la columna 0 por que no existe".
:- test columns(X,C) : (X = [[s(0),s(s(0))],[s(s(s(0)))]]) + fails #"Falla por que no es un building".

:- pred total_people(X,T) #"Retorna en @var{T} el total de las personas que viven en el building @var{X}. @includedef{total_people/2}".
total_people([],0).
total_people([E|L],T) :- building([E|L]),total_people_piso(E,T1),total_people(L,T2),suma(T1, T2, T).

:- test total_people(X,T) : (X = [[0,0],[0,0]]) => (T = 0) + not_fails #"Retorna el total de perosnas del edificio".
:- test total_people(X,T) : (X = [[s(0),0],[0,0]]) => (T = s(0)) + not_fails #"Retorna el total de perosnas del edificio".
:- test total_people(X,T) : (X = [[s(0),0],[0,s(0)]]) => (T = s(s(0))) + not_fails #"Retorna el total de perosnas del edificio".
:- test total_people(X,T) : (X = [[s(0),0],[s(0)]]) + fails #"Falla por que no es un building".

:- pred average(X,A) #"Retorna en @var{A} la media de las personas que viven en cada vivienda el building @var{X}. @includedef{average/2}".
average(X,A) :- building(X),total_people(X,T1),total_viviendas(X,T2),divEnt(T1,T2,A).

:- test average(X,A) : (X = [[s(0),s(0)],[s(0),s(0)]]) => (A = s(0)) + not_fails #"Retorna la media de personas por vivienda".
:- test average(X,A) : (X = [[0,0],[0,0]]) => (A = 0) + not_fails #"Retorna la media de personas por vivienda".
:- test average(X,A) : (X = [[s(0),0],[s(0)]]) + fails #"Falla por que no es un building".

% ****************** FUNCIONES AUXILIARES ****************** 
:- pred building_same(X,N) #"Funcion auxiliar que comprueba que todos los pisos tengan el mismo numero @var{N} de viviendas. @includedef{building_same/2}".
building_same([X],N) :- piso(X),count(X,N).
building_same([E|X],N) :- basic_building([E|X]),count(E, N),building_same(X, N).

:- pred columns_trans(X,Y,C) #"Funcion auxiliar que recorre la matriz fila a fila. @includedef{columns_trans/3}".
columns_trans([], _, []).
columns_trans([_|L], X, [E|Ls]) :- lists_to_column(X, E, X1),columns_trans(L, X1, Ls).

:- pred lists_to_column(X,Y,C) #"Funcion auxiliar que trasnforma una fila en una columna y la retorna en @var{C}. @includedef{lists_to_column/3}".
lists_to_column([], [], []).
lists_to_column([[E|L]|L1], [E|L2], [L|L3]) :- lists_to_column(L1, L2, L3).

:- pred total_people_piso(X,T) #"Funcion auxiliar que retorna en @var{T} el total de las personas que viven en cada piso @var{X}. @includedef{total_people_piso/2}".
total_people_piso([],0).
total_people_piso([E|L],T) :- total_people_piso(L,T1), suma(E, T1, T).


:- pred total_viviendas(X,T) #"Funcion auxiliar que retorna en @var{T} el total de viviendas en el building @var{X}. @includedef{total_viviendas/2}".
total_viviendas([],0).
total_viviendas([E|L],T) :- count(E,T1),total_viviendas(L,T2),suma(T1, T2, T).


:- pred suma(X,Y,Z) #"Funcion auxiliar que suma las @var{X} y @var{Y} y retoran el valor en @var{Z}. @includedef{suma/3}".
suma(0, X, X) :- natural(X).
suma(s(X), Y, s(Z)) :- suma(X, Y, Z).


:- pred producto(X,Y,Z) #"Funcion auxiliar que realiza el producto de las @var{X} y @var{Y} y retoran el valor en @var{Z}. @includedef{producto/3}".
producto(s(0), Y, Y) :- natural(Y).
producto(s(X), Y, Z) :- suma(Y, Z0, Z),producto(X, Y, Z0).


:- pred mod(X,Y,Z) #"Funcion auxiliar que realiza el modulo de las @var{X} y @var{Y} y retoran elvalor en @var{Z}. @includedef{mod/3}".
mod(X,Y,X) :- menor_que(X,Y).
mod(X,Y,Z) :- suma(X1,Y,X),mod(X1,Y,Z).


:- pred divEnt(X,Y,Z) #"Funcion auxiliar que realiza el la division sin decimales de las @var{X} y @var{Y} y retoran elvalor en @var{Z}. @includedef{divEnt/3}".
divEnt(X,Y,0) :- menor_que(X,Y).
divEnt(X,Y,Z) :- mod(X,Y,R),suma(R,H,X),producto(Y,Z,H).


:- pred mayor_que(X,Y) #"Funcion auxiliar que comprueba si @var{X} es mayor que la @var{Y}. @includedef{mayor_que/2}".
mayor_que(s(X),0) :- natural(X).
mayor_que(s(X),s(Y)) :- mayor_que(X,Y).


:- pred menor_que(X,Y) #"Funcion auxiliar que comprueba si @var{X} es mayor que la @var{Y}. @includedef{menor_que/2}".
menor_que(0,s(X)) :- natural(X).
menor_que(s(X),s(Y)) :- menor_que(X,Y).


:- pred lista(X) #"Funcion auxiliar que define si @var{X} es una lista. @includedef{lista/1}".
lista([]).
lista([_|L]) :- lista(L).


:- pred add_last(X,Y,Z) #"Funcion auxiliar que concatena un valor @var{Y} a la lista @var{X} y devuelve la nueva lista en @var{Z}. @includedef{add_last/3}".
add_last([], Y, Y) :- lista(Y).
add_last([X|XS], YS, [X|ZS]) :- add_last(XS, YS, ZS).


:- pred count(X,Y) #"Funcion auxiliar que cuenta el numero de elementos de @var{X} devuelve el valor en @var{Y}. @includedef{count/2}".
count([],0).
count([_|L],s(X)) :- count(L, X).


:- pred get_nth(X,N,C) #"Funcion auxiliar que busca el valor n-esimo @var{N} en una lista @var{X} y lo retorna en @var{C}. @includedef{get_nth/3}".
get_nth([E|_],0,E). 
get_nth([_|L],N,C) :- mayor_que(N,0),suma(N1,s(0),N),get_nth(L,N1,C).
